const express = require('express');

const router = express.Router();

const FriendCtrl = require('../controller/friends');
const AuthHelper = require('../Helpers/AuthHelper');

router.post('/followe-user', AuthHelper.VerifyToken, FriendCtrl.FollowUser);
router.post('/unfollowe-user', AuthHelper.VerifyToken, FriendCtrl.UnFollowUser);
router.post('/mark/:id', AuthHelper.VerifyToken, FriendCtrl.MarkNotification);
router.post('/mark-all', AuthHelper.VerifyToken, FriendCtrl.MarkAllNotifications);

module.exports = router;