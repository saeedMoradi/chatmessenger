const cloudinary = require('cloudinary');
const HttpStatus = require('http-status-codes');

const User = require('../models/userModels');

cloudinary.config({
    cloud_name: 'dit1praji',
    api_key: '492657482156221',
    api_secret: 'zqFKP0kseuE-nmmbt_WQPPHGPso'
});

module.exports = {
    UploadImage(req, res) {
        // console.log(req.body)
        cloudinary.uploader.upload(req.body.image, async result => {
            console.log(result);

            await User.update(
                {
                _id: req.user._id
                },
                {
                    $push: {
                        images: {
                            imgId: result.public_id,
                            imgVersion: result.version
                        }
                    }
                }).then(() =>
                    res 
                        .status(HttpStatus.OK)
                        .json({message: 'Image uploaded successfully'})
                ).catch(err =>
                    res 
                        .status(HttpStatus.INTERNAL_SERVER_ERROR)
                        .json({message: err}
                )
            )
        })
    },

    async SetDefaultImage(req, res) {
        const {imgId, imgVersion} = req.params;

        await User.update(
            {
                _id: req.user._id
            },
            {
                picId: imgId,
                picVersion: imgVersion
            }
        )
            .then(() =>
                res 
                    .status(HttpStatus.OK)
                    .json({message: 'Image uploaded successfully'})
            )
            .catch(err =>
                res 
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .json({message: err})
            )
        
    }
};