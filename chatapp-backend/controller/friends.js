const HttpStatus = require('http-status-codes');

const User = require('../models/userModels');

module.exports = {
    FollowUser(req, res) {
        const followUser = async () => {
            await User.update({
                _id: req.user._id,
                'following.userFollowed': { $ne: req.body.userFollowed } 
            },{
                $push: {
                    following: {
                        userFollowed: req.body.userFollowed
                    }
                }
            });

            await User.update({
                _id: req.body.userFollowed,
                'following.follower': { $ne: req.user._id }
            },{
                $push: {
                    followers: {
                        follower: req.user._id
                    },
                    notifications: {
                        senderId: req.user._id,
                        message: `اکنون شما را به عنوان دنبال کننده انتخاب کرد ${req.user.username} `,
                        created: new Date(),
                        viewProfile: false
                    }
                }
            });
        };

        followUser()
            .then(() => {
                res 
                    .status(HttpStatus.OK).json({ message: 'Following user now' });
            })
            .catch(err => {
                res 
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .json({ message: err });
            });
    },

    UnFollowUser(req, res){
        const unFollowUser = async () => {
            await User.update({
                _id: req.user._id,
            },{
                $pull: {
                    following: {
                        userFollowed: req.body.userFollowed, 
                    }
                }
            });

            await User.update({
                _id: req.body.userFollowed,
            },{
                $pull: {
                    followers: {
                        follower: req.user._id
                    }
                }
            });
        };

        unFollowUser()
            .then(() => {
                res 
                    .status(HttpStatus.OK).json({ message: 'UnFollowing user now' });
            })
            .catch(err => {
                res 
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .json({ message: err });
            });
    },

    async MarkNotification(req, res) {
        // console.log(req.body); 
        if (!req.body.deleteValue) {
            await User.updateOne({
                _id: req.user._id,
                'notifications._id': req.params.id
            },{
                $set: {'notifications.$.read': true}
            })
            .then(() => {
                res
                    .status(HttpStatus.OK).json({message: 'Marked as read'});
            }).catch (err =>{
                res 
                    .status(HttpStatus.INTERNAL_SERVER_ERROR).json({message: err})
            })
        }else {
            await User.update(
                {
                    _id: req.user._id,
                    'notifications._id': req.params.id
                },
                {
                    $pull: {
                        notifications: { _id: req.params.id }
                    }
                }
            )
            .then(() => {
                res
                    .status(HttpStatus.OK).json({message: 'Delete successfull'});
            }).catch (err =>{
                res 
                    .status(HttpStatus.INTERNAL_SERVER_ERROR).json({message: err})
            })
        }
    },

    async MarkAllNotifications(req, res) {
        await User.update({
            _id: req.user._id
        },
        {$set: {'notifications.$[elem].read': true}},
        { arrayFilters: [{ 'elem.read': false }], multi: true }
        )
        .then(() => {
            res
                .status(HttpStatus.OK).json({message: 'Marked all successfull'});
        }).catch (err =>{
            res 
                .status(HttpStatus.INTERNAL_SERVER_ERROR).json({message: err})
        });
    }    

}