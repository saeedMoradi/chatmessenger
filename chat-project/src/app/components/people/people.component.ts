import { Component, OnInit } from '@angular/core';
import { UsesService } from '../../services/users.service';
import { TokenService } from '../../services/token.service';
import _ from 'lodash';
import io from 'socket.io-client';
import { Router } from '@angular/router';


@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.scss']
})
export class PeopleComponent implements OnInit {
  socket: any;
  users = [];
  loggedInUser: any;
  userArr = [];
  onlineusers = [];

  constructor(
    private userService: UsesService,
    private tokenService: TokenService,
    private router: Router,
    ) {
    this.socket = io('http://localhost:3000');
  }

  ngOnInit() {
    this.loggedInUser = this.tokenService.GetPayload();
    this.GetUsers();
    this.GetUser();

    this.socket.on('refreshPage', () => {
      this.GetUsers();
      this.GetUser();
    })

  }

  GetUsers() {
    this.userService.GetAllUsers().subscribe(data =>{
      // console.log(data);
      _.remove(data.result, {username: this.loggedInUser.username});
      this.users = data.result; 
    })
  }

  GetUser() {
    this.userService.GetUserById(this.loggedInUser._id).subscribe(data =>{
      // console.log(data);
      this.userArr = data.result.following;
    })
  }

  FollowUser(user) {
    // console.log(user);
    this.userService.FollowUser(user._id).subscribe(data => {
      // console.log(data);
      // this.userArr = data.result.following;
      this.socket.emit('refresh', {});
    });
  }

  ViewUser(user) {
    // console.log(user);
    this.router.navigate([user.username]);
    if (this.loggedInUser.username !== user.username) {
      this.userService.ProfileNotifications(user._id).subscribe(data => {
        this.socket.emit('refresh', {});
      }, err => console.log(err));
    }
  }

  CheckInArray(arr, id) {
    const result = _.find(arr, ['userFollowed._id', id]);
    if (result) {
      return true;
    } else {
      return false;
    }
  }

  online(event) {
    this.onlineusers = event;
  }

  checkIfOnline(name) {
    const result = _.indexOf(this.onlineusers, name);
    if (result > -1) {
      return true;
    } else {
      return false;
    }
  }

}
