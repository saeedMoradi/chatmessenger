import { Component, OnInit } from '@angular/core';
import io from 'socket.io-client';
import { TokenService } from 'src/app/services/token.service';
import { UsesService } from 'src/app/services/users.service';
import * as moment from 'moment';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {

  following = [];
  user: any;

  socket: any;

  notifications = [];

  constructor(private userService: UsesService, private tokenService: TokenService) {
    this.socket = io('http://localhost:3000');
  }

  ngOnInit() {
    this.user = this.tokenService.GetPayload();

    this.GetUser();

    this.socket.on('refreshPage',()=>{
      this.GetUser();
    })

  }

  GetUser() {
    this.userService.GetUserById(this.user._id).subscribe(data => {
      console.log(data);
      this.notifications = data.result.notifications.reverse();
    }, err => console.log(err));

  }


  markNotification(data) {
    // console.log(data);
    this.userService.MarkNotification(data._id).subscribe(data => {
      // console.log(data);
      this.socket.emit('refresh', {});
    })
  }

  deleteNotification(data) {
    // console.log(data);
    this.userService.MarkNotification(data._id, true).subscribe(data => {
      // console.log(data);
      this.socket.emit('refresh', {});
    })

  }


  TimeFromNow(time){
    return moment(time).fromNow();
  }


}
