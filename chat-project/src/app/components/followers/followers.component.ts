import { Component, OnInit } from '@angular/core';
import { UsesService } from 'src/app/services/users.service';
import { TokenService } from 'src/app/services/token.service';
import io from 'socket.io-client';

@Component({
  selector: 'app-followers',
  templateUrl: './followers.component.html',
  styleUrls: ['./followers.component.scss']
})
export class FollowersComponent implements OnInit {

  followers = [];
  user: any;

  socket: any;

  constructor(private userService: UsesService, private tokenService: TokenService) {
    this.socket = io('http://localhost:3000');
  }

  ngOnInit() {
    this.user = this.tokenService.GetPayload();
    this.GetUser();

    this.socket.on('refreshPage', () =>{
      this.GetUser();
    })
  }


  GetUser() {
    this.userService.GetUserById(this.user._id).subscribe(data => {
      this.followers = data.result.followers;
      // console.log(data.result.followers);
    }, err => console.log(err));
  }


}
