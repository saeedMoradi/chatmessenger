import { Component, OnInit } from '@angular/core';
import { TokenService } from '../../services/token.service';
import { UsesService } from '../../services/users.service';
import io from 'socket.io-client';


@Component({
  selector: 'app-side',
  templateUrl: './side.component.html',
  styleUrls: ['./side.component.scss']
})
export class SideComponent implements OnInit {

  socket: any;
  user: any;
  userData: any;

  constructor(private tokenService: TokenService, private userService: UsesService) {

    this.socket = io('http://localhost:3000');

  }

  ngOnInit() {
    this.user = this.tokenService.GetPayload();
    this.GetUser();
    this.socket.on('refreshPage', () => {
      this.GetUser();
    })
  }

  GetUser() {
    this.userService.GetUserById(this.user._id).subscribe(data => {
      this.userData = data.result;
      // console.log(data);
    })
  }

}
