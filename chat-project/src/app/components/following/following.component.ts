import { Component, OnInit } from '@angular/core';
import { TokenService } from '../../services/token.service';
import { UsesService } from '../../services/users.service';
import io from 'socket.io-client';


@Component({
  selector: 'app-following',
  templateUrl: './following.component.html',
  styleUrls: ['./following.component.scss']
})
export class FollowingComponent implements OnInit {

  following = [];
  user: any;

  socket: any;

  constructor(private userService: UsesService, private tokenService: TokenService) {
    this.socket = io('http://localhost:3000');
  }

  ngOnInit() {
    this.user = this.tokenService.GetPayload();
    this.GetUser();

    this.socket.on('refreshPage', () =>{
      this.GetUser();
    })

  }

  GetUser() {
    this.userService.GetUserById(this.user._id).subscribe(data => {
      this.following = data.result.following;
      // console.log(data);
    }, err => console.log(err));
  }


  UnFollowUser(user){
    // console.log(user);
    this.userService.UnFollowUser(user._id).subscribe(data =>{
      // console.log(data);
      this.socket.emit('refresh', {});
    })
  }

}
