import { Component, OnInit } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import { UsesService } from 'src/app/services/users.service';
import { TokenService } from 'src/app/services/token.service';
import io from 'socket.io-client';

const URL = 'http://localhost:3000/api/chatapp/upload-image';

@Component({
  selector: 'app-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.scss']
})
export class ImagesComponent implements OnInit {


  uploader: FileUploader = new FileUploader({
    url: URL,
    disableMultipart: true
  });
  user: any;
  selectedFile: any;
  images = [];

  socket: any;

  constructor(private usersService: UsesService, private tokenService: TokenService) {
    this.socket = io('http://localhost:3000');
  }

  ngOnInit() {
    this.user = this.tokenService.GetPayload();
    this.GetUser();

    this.socket.on('refreshPage', () =>{
      this.GetUser();
    })
  }

  GetUser() {
    this.usersService.GetUserById(this.user._id).subscribe(data =>{
      // console.log(data.result.images);
      this.images = data.result.images;
      // console.log(this.images);
    })
  }

  OnFileSelected(event){
    // console.log(event);
    const file: File = event[0];

    this.ReadAsBase64(file).then(result => {
        this.selectedFile = result;
    }).catch(err => console.log(err));
  }

  Upload() {
    // console.log(this.selectedFile);
    if (this.selectedFile) {
      this.usersService.AddImage(this.selectedFile).subscribe(data => {
        // console.log(data);
        this.socket.emit('refresh', {});
        const filePath = <HTMLInputElement>document.getElementById('filePath');
        filePath.value = '';
      }, err => console.log(err)
      );
    }
  }

  SetProfileImage(image) {
    // console.log(image);
    this.usersService.SetDefaultImage(image.imgId, image.imgVersion).subscribe(data => {
      this.socket.emit('refresh', {});
    },
      err => console.log(err)
    )
  }

  ReadAsBase64(file): Promise<any> {
    const reader = new FileReader();
    const fileValue = new Promise((resolve, reject) => {
      reader.addEventListener('load', () => {
        resolve(reader.result);
      });

      reader.addEventListener('error', event => {
        reject(event);
      });

      reader.readAsDataURL(file);

    });

    return fileValue;
  }

}
